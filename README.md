# DynBHSC

Papers, slides, and other animals

# Contacts

You can contact me either via email  
_mario.spera@northwestern.edu_    
_mario.spera@live.it_    
or you can stop by my **temporary office** (room 510, 5th floor)    
or you can also open a **new issue** on the this git repository   
